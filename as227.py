#!/usr/bin/env python3
from enum import Enum, IntEnum
import serial


# This is based on @lkwi's post on elektroda.pl.
# https://www.elektroda.pl/rtvforum/viewtopic.php?p=7981931#7981931
#
# Note: to display multiple pages a TimeWindow needs to be defined.
#       A TimeWindow spanning from 00:00 to 00:00 will be constantly used.


class DisplayMethod(Enum):
	CYCLIC = b'\x01'
	IMMEDIATE = b'\x02'
	OPEN_FROM_RIGHT = b'\x03'
	OPEN_FROM_LEFT = b'\x04'
	OPEN_FROM_CENTER = b'\x05'
	OPEN_TO_CENTER = b'\x06'
	COVER_FROM_CENTER = b'\x07'
	COVER_FROM_RIGHT = b'\x08'
	COVER_FROM_LEFT = b'\x09'
	COVER_TO_CENTER = b'\x0a'
	SCROLL_UP = b'\x0b'
	SCROLL_DOWN = b'\x0c'
	INTERLACE_TO_CENTER = b'\x0d'
	INTERLACE_COVER = b'\x0e'
	COVER_UP = b'\x0f'
	COVER_DOWN = b'\x10'
	SCAN_LINE = b'\x11'
	EXPLODE = b'\x12'
	PAC_MAN = b'\x13'
	FALL_AND_STACK = b'\x14'
	SHOOT = b'\x15'
	FLASH = b'\x16'
	RANDOM = b'\x17'
	SLIDE_IN = b'\x18'
DEFAULT_DISPLAY_METHOD = DisplayMethod.CYCLIC


class Color(IntEnum):
	RED = 0
	BRIGHT_RED = 1
	ORANGE = 2
	BRUGHT_ORANGE = 3
	YELLOW = 4
	BRIGHT_YELLOW = 5
	GREEN = 6
	BRIGHT_GREEN = 7
	LAYER_MIX = 8
	BRIGHT_LAYER_MIX = 9
	VERTICAL_MIX = 10
	SAW_TOOTH_MIX = 11
	GREEN_ON_RED = 12
	RED_ON_GREEN = 13
	ORANGE_ON_RED = 14
	YELLOW_ON_GREEN = 15


class Font(IntEnum):
	H5W6 = 0
	H5W11 = 1
	H7W6 = 2
	H7W11 = 3
	H7W9 = 4
	H7W17 = 5
	SMALL = 6


class ScrollSpeed(IntEnum):
	FASTEST = 0
	FASTER = 1
	FAST = 2
	MEDIUM = 3
	SLOW = 4
	SLOWER = 5
	SLOWEST = 6


class PauseTime(IntEnum):
	T2S = 0
	T3S = 1
	T4S = 2
	T6S = 3
	T10S = 4
	T20S = 5
	T30S = 6
	T1M = 7


class Symbol(IntEnum):
	SUN = 0
	SYM1 = 1
	UMBRELLA = 2
	CLOCK = 3
	PHONE = 4
	GLASSES = 5
	FAUCET = 6
	ROCKET = 7
	SYM8 = 8
	KEY = 9
	SHIRT = 10
	HELLICOPTER = 11
	CAR = 12
	TANK = 13
	HOUSE = 14
	TEAPOT = 15
	TREES = 16
	DUCK = 17
	MOTORCYCLE = 18
	BICYCLE = 19
	CROWN = 20
	SYM21 = 21
	ARROW_RIGHT = 22
	ARROW_LEFT = 23
	ARROW_DOWN_LEFT = 24
	ARROW_UP_LEFT = 25
	MUG = 26
	CHAIR = 27
	SHOE = 28
	GLASS = 29


class Graphic(IntEnum):
	CITY = 0
	PARKING = 1
	BAR = 2
	TELEPHONE = 3
	NATURE = 4
	SHIP = 5
	SWIM = 6
	CAT = 7


class Animation(IntEnum):
	MERRY_XMAS = 0
	HAPPY_NEW_YEAR = 1
	JULY_4TH = 2
	HAPPY_EASTER = 3
	HAPPY_HALLOWEEN = 4
	DONT_DRINK_AND_DRIVE = 5
	NO_SMOKING = 6
	WELCOME = 7


class Page:
	def __init__(self, display_method:DisplayMethod=DEFAULT_DISPLAY_METHOD):
		self.display_method = display_method
		self.clear()

	def set_display_method(self, display_method:DisplayMethod):
		self.display_method = display_method

	def clear(self):
		self.data = b''
		return self

	def add_text(self, txt):
		self.data += txt.encode()
		return self

	def next_line(self):
		self.data += b'\xff'
		return self

	def _next_line_cond(self):
		if self.data[-1] != 0xff:
			self.next_line()

	def add_raw_command(self, cmd):
		self.data += bytearray([0xef, cmd])
		return self

	def set_font(self, font : Font):
		return self.add_raw_command(0xa0 + font)

	def set_color(self, color : Color):
		return self.add_raw_command(0xb0 + color)

	def set_scroll_speed(self, scroll_speed:ScrollSpeed):
		return self.add_raw_command(0xc0 + scroll_speed)

	def set_pause_time(self, pause_time:PauseTime):
		return self.add_raw_command(0xc8 + pause_time)

	def add_symbol(self, symbol:Symbol):
		return self.add_raw_command(0x60 + symbol)

	def set_graphic(self, graphic:Graphic):
		return self.add_raw_command(0xd0 + graphic)

	def set_animation(self, animation:Animation):
		return self.add_raw_command(0x90 + animation)

	def add_time(self, standalone=True):
		if standalone:
			self._next_line_cond()
		self.add_raw_command(0x80)
		if standalone:
			self.next_line()
		return self

	def add_date(self, standalone=True):
		if standalone:
			self._next_line_cond()
		self.add_raw_command(0x81)
		if standalone:
			self.next_line()
		return self

	def add_temperature(self):
		return self.add_raw_command(0x82)

	def get_raw(self, page_number):
		self._next_line_cond()
		return b'\x01' + _format_digits(page_number) + self.display_method.value + self.data


def _format_digits(*vals):
	return b''.join(b'%02d' % val for val in vals)


class TimeWindow:
	def __init__(self, start, end, pages=[]):
		self._validate_time(start)
		self._validate_time(end)
		self.start = start
		self.end = end
		self.pages = list(pages)

	def _validate_time(self, time):
		if type(time) is not tuple or len(time) != 2:
			raise TypeError('time must be tuple with 2 integers')

	def get_raw(self):
		return b'\x02\x00\x00' + _format_digits(*(self.start + self.end + tuple(self.pages)))


class Controller:
	HEADER = b'\x00\xff\xff\x00\x0b' + bytearray(range(128)) + b'\xff'
	END = b'\xff\x00'

	def __init__(self):
		self.s = None
		self.clear()

	def clear(self):
		self.pages = []
		self.time_window = None

	def add_page(self, page, display_method:DisplayMethod=DEFAULT_DISPLAY_METHOD, color=None, font=None):
		if type(page) is str:
			txt = page
			page = Page(display_method)
			if color is not None:
				page.set_color(color)
			if font is not None:
				page.set_font(font)
			page.add_text(txt)
		self.pages.append(page)

	def set_time_window(self, tw:TimeWindow):
		self.time_window = tw

	def open(self, port):
		self.s = serial.Serial(port, 2400)

	def close(self):
		self.s.close()

	def update(self):
		self.s.write(self.HEADER)
		for pgi, page in enumerate(self.pages, 1):
			if pgi > 1:
				self.s.write(b'\xff\xff\xff')
			self.s.write(page.get_raw(pgi))
		if self.time_window is not None:
			self.s.write(b'\xff\xff\xff')
			self.s.write(self.time_window.get_raw())
		self.s.write(self.END)


def main():
	def enum2choices(enum):
		return list(e.name.lower() for e in enum)

	import argparse
	ap = argparse.ArgumentParser(description='AS227 controller')
	ap.add_argument('-P', '--port', type=str, default='/dev/ttyUSB0', help='port')
	ap.add_argument('-m', '--method', type=str, choices=enum2choices(DisplayMethod), help='display method')
	ap.add_argument('-f', '--font', type=str, choices=enum2choices(Font), help='font size')
	ap.add_argument('-c', '--color', type=str, choices=enum2choices(Color), help='font color')
	ap.add_argument('-s', '--speed', type=str, choices=enum2choices(ScrollSpeed), help='scroll speed')
	ap.add_argument('-p', '--pause', type=str, choices=enum2choices(PauseTime), help='pause time')
	ap.add_argument('message', type=str, help='message to display')
	args = ap.parse_args()

	ac = Controller()
	pg = Page()
	if args.method is not None:
		pg.set_display_method(DisplayMethod[args.method.upper()])
	if args.font is not None:
		pg.set_font(Font[args.font.upper()])
	if args.color is not None:
		pg.set_color(Color[args.color.upper()])
	if args.speed is not None:
		pg.set_scroll_speed(ScrollSpeed[args.speed.upper()])
	if args.pause is not None:
		pg.set_pause_time(PauseTime[args.pause.upper()])
	pg.add_text(args.message)
	ac.add_page(pg)

	ac.open(args.port)
	ac.update()
	ac.close()

if __name__ == '__main__':
	main()
